# Pelican template used in my blog

Feel free to re-use it. I mit licensed it.

Pull requests are welcome, bear in mind however that I don't plan to make this 
template more **generic** as it is created for single project. 

Choices I made: 

* Use nice open-source monotype font [The League Mono font](https://www.theleagueofmoveabletype.com/league-mono>)
  ([please see all their fonts](<https://www.theleagueofmoveabletype.com/>))
* Keep no (or almost no JS). 
* Keep as little CSS as possible (`boostrap-reset` is used, but rest of the css is done by hand without bootstrap classes). 
* Used ``scss``, to compile it just `cd static && sassc main.scss -m main.css`.

  If you want to configure File Watcher in Jetbrains IDE use: 
  
  ``$FileName$ -m $FileNameWithoutExtension$.css`` 